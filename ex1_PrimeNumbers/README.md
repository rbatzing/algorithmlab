# Experiment 1: Searching for Prime Neighbors

Comparison of 3 different class definitions for identifying prime numbers and neighboring prime numbers. 

## Research Question:

     How much impact can the selection of an algorithm have on the speed of computer software?
	 
	 Specifically, in an era of fast computers with lots of memory, how important is it to develop good algorithms (using the listing of prime numbers as an example)? (i.e., is just getting it to work, good enough?)

## Classes:

This research focuses on 3 different Ruby classes. Each class definition has the same API with matching methods. There are some small variations in the way the attributes are used.

* **Class PrimeA:** Uses the built-in prime module of Ruby
* **Class PrimeB:** Check each number for factors by using modulus
* **Class PrimeC:** Uses a matrix of logic values to indicate if the number is prime. The matrix entries are marked for each multiple of every prime number found.

## Methods:

* **new(max)** - constructor method for a range of prime number from 2 to max - 1.
* **find_primes()** - find all prime numbers within a range
* **find_neighbors()** - find all pairs of prime neighbors
* **count_primes()** - count the number of prime numbers within the range
* **count_neighbors()** - count the number of neighboring pairs
* **show_neighbors()** - display the list of neighbor pairs
* **show_primes()** - display the list of all prime numbers
* **prime?(num)** - check if a number is a prime number or not
* **to_s()** - print a summary of the prime numbers

## Attributes:

* **@primes** - list of primes or the logic identifying if a number is prime
* **@max** - the size of the area
* **@neighbors** - list of pairs of neighboring primes 

## Running the Experiment:

Copy the following lines to a bash window

	echo benchmark.txt
	ruby primes_benchmark.rb > benchmark.txt

    echo Creating profile.txt
	ruby primesA_profile.rb > profile.txt
	ruby primesB_profile.rb >> profile.txt
	ruby primesC_profile.rb >> profile.txt

	echo Creating memprofile.txt
	ruby primesA_memprofile.rb > memprofile.txt
	ruby primesB_memprofile.rb >> memprofile.txt
	ruby primesC_memprofile.rb >> memprofile.txt 

	echo Creating test.txt
	primesA.rb primesB.rb primesC.rb 
	ruby primesA.rb > test.txt
	ruby primesB.rb >> test.txt
	ruby primesC.rb >> test.txt


## Experiment Guidelines

1. Open a new project in Overleaf and describe the purpose and your thoughts about the research question of this experiment. (Introduction)

2. Develop a flowchart or pseudocode to describe how the find_primes() methods work for each of the class definitions. Explain key features of your diagram or pseudocode. (Introduction)

2. Run the test files either by using the makefile or by executing the above commands within the working directory of this experiment. (Methodology) 

3. Review the resulting txt files to verify the flowchart and to determine how much time is spent in each method. (Results)

4. Determine the relationship between the size of the number set tested and the time required. You will need to use log-log graphs. (Results)

5. Summary the differences between the algorithms used in the 3 classes and the impact these differences have on the processing time (Discussion)

6. Finalize the documentation adding a title, author list, abstract, keywords and bibliography. Submit your final paper in the form of the url to the shared research project folder in Overleaf.com.  

