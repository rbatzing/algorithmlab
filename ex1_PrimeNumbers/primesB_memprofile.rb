require_relative "primesB"
require 'memory_profiler'

puts "\n==== PrimesB ==============\n"
MemoryProfiler.start

# run your code
test = PrimesB.new(100000)
puts test.count_primes

report = MemoryProfiler.stop
report.pretty_print