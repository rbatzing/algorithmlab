require_relative "primesA"
require 'profiler'

puts "\n==== PrimesA ==============\n"
Profiler__::start_profile

test = PrimesA.new(100000)
puts test.count_primes

Profiler__::stop_profile
Profiler__::print_profile(STDOUT)
