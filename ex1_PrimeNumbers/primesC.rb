class PrimesC
	attr_accessor :primes, :max, :neighbors

  def initialize(max)
		@max = max
	end
	
	def find_primes
		@primes = Array.new(max,true)
		@primes[0] = false
		@primes[1] = false
		(2...@max).to_a.each do |i|
			if @primes[i]
				x = i + i
				while x < @max do
					@primes[x] = false
					x = x + i
				end
			end	  
		end
	end
	
	def find_neighbors
	  find_primes if @primes.nil?
		@neighbors = Array.new
		(1...(@max-2)).to_a.each do |i|
			if @primes[i] && @primes[i+2]
			  @neighbors << [i,i+2]
			end
		end
	end
	
	def count_primes
		find_primes if @primes.nil?
		cnt = 0
		@max.times do |i|
		  cnt += 1 if @primes[i]
		end
		return cnt
	end
	
	def count_neighbors
	  find_neighbors if @neighbors.nil?
	  return @neighbors.length
	end
	
	def show_neighbors
	  output = "Neighbors:\n"
    cnt = 1		
		@neighbors.each do |x|
			output += "#{x}, "
		  if cnt % 4 == 0
			  output += "\n"
			end
			cnt += 1
	  end
		output += "\n"
		return output
	end
	
	def show_primes
		find_primes if @primes.nil?
		output = "Primes: \n"
		cnt = 1
		@primes.length.times do |p| 
			if @primes[p]
				output += "#{p}, "
				if cnt % 5 == 0
					output += "\n"
				end
			end
			cnt += 1
		end
		output += "\n"
		return output
	end
	
	def prime?(num)
		find_primes if @primes.nil?
		if (num < 1) || (num >= @max)
			puts "ERROR: #{num} outside operational range"
	    return false
	  else
	    return @primes[num]
	  end
	end
	
	def to_s
    output = "Number of Primes: #{count_primes}\n"
	  output += "Number of Prime Neighbors: #{count_neighbors}\n"
		return output		
	end
end

if __FILE__ == $0
	puts
	puts "=== Testing PrimeC" + "=" * 15
	test = PrimesC.new(10000)
	test.find_neighbors
	puts test
	puts "=========================="
  test2 = PrimesC.new(20)
	(2...20).to_a.each do |x|
		print "\t#{x} - #{test2.prime?(x)}"
		puts  if x % 5 == 0
  end
  puts
  puts "=========================="
	puts test2
	puts test2.show_primes
	puts test2.show_neighbors
	puts "=========================="
end
