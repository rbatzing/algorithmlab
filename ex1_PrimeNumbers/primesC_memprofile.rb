require_relative "primesC"
require 'memory_profiler'

puts "\n==== PrimesA ==============\n"
MemoryProfiler.start

# run your code
test = PrimesC.new(100000)
puts test.count_primes

report = MemoryProfiler.stop
report.pretty_print