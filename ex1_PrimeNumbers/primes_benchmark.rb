
require_relative "primesA"
require_relative "primesB"
require_relative "primesC"

require 'benchmark'
include Benchmark

puts "\nPrimesA Class results " + "=" * 36
bm(7) do |x|
  x.report("10"){100.times{  PrimesA.new(10).count_primes }}
  x.report("32"){100.times{  PrimesA.new(32).count_primes }}
  x.report("100"){100.times{  PrimesA.new(100).count_primes }}
  x.report("316"){100.times{  PrimesA.new(316).count_primes }}
  x.report("1000"){100.times{  PrimesA.new(1000).count_primes }}
  x.report("3162"){100.times{  PrimesA.new(3162).count_primes }}
  x.report("10000"){100.times{  PrimesA.new(10000).count_primes }}
  x.report("31623"){100.times{  PrimesA.new(31623).count_primes }}
  x.report("100000"){100.times{ PrimesA.new(100000).count_primes }}
  x.report("316228"){100.times{ PrimesA.new(316228).count_primes }}
	x.report("1000000"){100.times{ PrimesA.new(1000000).count_primes }}
end

puts "\nPrimesB Class results " + "=" * 36
bm(7) do |x|
  x.report("10"){100.times{  PrimesB.new(10).count_primes }}
  x.report("32"){100.times{  PrimesB.new(32).count_primes }}
  x.report("100"){100.times{  PrimesB.new(100).count_primes }}
  x.report("316"){100.times{  PrimesB.new(316).count_primes }}
  x.report("1000"){100.times{  PrimesB.new(1000).count_primes }}
  x.report("3162"){100.times{  PrimesB.new(3162).count_primes }}
  x.report("10000"){100.times{  PrimesB.new(10000).count_primes }}
  x.report("31623"){100.times{  PrimesB.new(31623).count_primes }}
  x.report("100000"){100.times{  PrimesB.new(100000).count_primes }}
  x.report("316228"){100.times{  PrimesB.new(316228).count_primes }}
  x.report("1000000"){100.times{  PrimesB.new(1000000).count_primes }}
end

puts "\nPrimesC Class results " + "=" * 36
bm(7) do |x|
  x.report("10"){100.times{  PrimesC.new(10).count_primes }}
  x.report("32"){100.times{  PrimesC.new(32).count_primes }}
  x.report("100"){100.times{  PrimesC.new(100).count_primes }}
  x.report("316"){100.times{  PrimesC.new(316).count_primes }}
  x.report("1000"){100.times{  PrimesC.new(1000).count_primes }}
  x.report("3162"){100.times{  PrimesC.new(3162).count_primes }}
  x.report("10000"){100.times{  PrimesC.new(10000).count_primes }}
  x.report("31623"){100.times{  PrimesC.new(31623).count_primes }}
  x.report("100000"){100.times{ PrimesC.new(100000).count_primes }}
  x.report("316228"){100.times{ PrimesC.new(316228).count_primes }}
	x.report("1000000"){100.times{ PrimesC.new(1000000).count_primes }}
end