require 'benchmark'
include Benchmark

puts "\nRuby Math op comparison " + "=" * 36

10.times do
	bm(3) do |x|
		x.report("="){10_000_000.times{  a = 50 }}
		x.report("+"){10_000_000.times{  a = 50 + 5 + 5 + 5 + 5 + 5}}
		x.report("-"){10_000_000.times{  a = 50 - 5 - 5 - 5 - 5 - 5}}
		x.report("x"){10_000_000.times{  a = 50 * 5 * 5 * 5 * 5 * 5}}
		x.report("/"){10_000_000.times{  a = 50 / 5 / 5 / 5 / 5 / 5}}
		x.report("%"){10_000_000.times{  a = 50 % 5 % 5 % 5 % 5 % 5}}
		x.report("%"){10_000_000.times{  a = 50 ^ 2 ^ 2 ^ 2 ^ 2 ^ 2}}
	end
end
