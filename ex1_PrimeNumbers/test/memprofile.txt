9592
Total allocated: 352192 bytes (73 objects)
Total retained:  195304 bytes (7 objects)

allocated memory by gem
-----------------------------------
    261696  prime
     90416  other
        80  singleton

allocated memory by file
-----------------------------------
    261696  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb
     89936  D:/git/algorithmlab/ex1/primesA.rb
       480  primesA_memprofile.rb
        80  c:/Ruby23-x64/lib/ruby/2.3.0/singleton.rb

allocated memory by location
-----------------------------------
    155744  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:449
    105112  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:426
     89712  D:/git/algorithmlab/ex1/primesA.rb:11
       440  primesA_memprofile.rb:8
       400  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:446
       400  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:451
       224  D:/git/algorithmlab/ex1/primesA.rb:12
        80  c:/Ruby23-x64/lib/ruby/2.3.0/singleton.rb:142
        40  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:135
        40  primesA_memprofile.rb:7

allocated memory by class
-----------------------------------
    349408  Array
      1200  Enumerator
       800  Range
       240  Hash
       120  Thread::Mutex
       104  RubyVM::Env
        80  Proc
        80  String
        40  Prime
        40  Prime::EratosthenesGenerator
        40  Prime::EratosthenesSieve
        40  PrimesA

allocated objects by gem
-----------------------------------
        62  prime
         9  other
         2  singleton

allocated objects by file
-----------------------------------
        62  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb
         5  primesA_memprofile.rb
         4  D:/git/algorithmlab/ex1/primesA.rb
         2  c:/Ruby23-x64/lib/ruby/2.3.0/singleton.rb

allocated objects by location
-----------------------------------
        40  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:449
        10  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:446
        10  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:451
         4  primesA_memprofile.rb:8
         3  D:/git/algorithmlab/ex1/primesA.rb:12
         2  c:/Ruby23-x64/lib/ruby/2.3.0/singleton.rb:142
         1  D:/git/algorithmlab/ex1/primesA.rb:11
         1  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:135
         1  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:426
         1  primesA_memprofile.rb:7

allocated objects by class
-----------------------------------
        33  Array
        20  Range
        10  Enumerator
         2  String
         1  Hash
         1  Prime
         1  Prime::EratosthenesGenerator
         1  Prime::EratosthenesSieve
         1  PrimesA
         1  Proc
         1  RubyVM::Env
         1  Thread::Mutex

retained memory by gem
-----------------------------------
    105112  prime
     90112  other
        80  singleton

retained memory by file
-----------------------------------
    105112  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb
     89712  D:/git/algorithmlab/ex1/primesA.rb
       400  primesA_memprofile.rb
        80  c:/Ruby23-x64/lib/ruby/2.3.0/singleton.rb

retained memory by location
-----------------------------------
    105112  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:426
     89712  D:/git/algorithmlab/ex1/primesA.rb:11
       360  primesA_memprofile.rb:8
        80  c:/Ruby23-x64/lib/ruby/2.3.0/singleton.rb:142
        40  primesA_memprofile.rb:7

retained memory by class
-----------------------------------
    194824  Array
       240  Hash
       120  Thread::Mutex
        40  Prime
        40  Prime::EratosthenesSieve
        40  PrimesA

retained objects by gem
-----------------------------------
         4  other
         2  singleton
         1  prime

retained objects by file
-----------------------------------
         3  primesA_memprofile.rb
         2  c:/Ruby23-x64/lib/ruby/2.3.0/singleton.rb
         1  D:/git/algorithmlab/ex1/primesA.rb
         1  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb

retained objects by location
-----------------------------------
         2  c:/Ruby23-x64/lib/ruby/2.3.0/singleton.rb:142
         2  primesA_memprofile.rb:8
         1  D:/git/algorithmlab/ex1/primesA.rb:11
         1  c:/Ruby23-x64/lib/ruby/2.3.0/prime.rb:426
         1  primesA_memprofile.rb:7

retained objects by class
-----------------------------------
         2  Array
         1  Hash
         1  Prime
         1  Prime::EratosthenesSieve
         1  PrimesA
         1  Thread::Mutex


Allocated String Report
-----------------------------------
         2  "9592"
         2  primesA_memprofile.rb:8


Retained String Report
-----------------------------------
9592
Total allocated: 1111824 bytes (8 objects)
Total retained:  90112 bytes (4 objects)

allocated memory by gem
-----------------------------------
   1111824  other

allocated memory by file
-----------------------------------
   1111344  D:/git/algorithmlab/ex1/primesB.rb
       480  primesB_memprofile.rb

allocated memory by location
-----------------------------------
   1021632  D:/git/algorithmlab/ex1/primesB.rb:10
     89712  D:/git/algorithmlab/ex1/primesB.rb:9
       440  primesB_memprofile.rb:8
        40  primesB_memprofile.rb:7

allocated memory by class
-----------------------------------
   1111304  Array
       240  Hash
       120  Thread::Mutex
        80  String
        40  PrimesB
        40  Range

allocated objects by gem
-----------------------------------
         8  other

allocated objects by file
-----------------------------------
         5  primesB_memprofile.rb
         3  D:/git/algorithmlab/ex1/primesB.rb

allocated objects by location
-----------------------------------
         4  primesB_memprofile.rb:8
         2  D:/git/algorithmlab/ex1/primesB.rb:10
         1  D:/git/algorithmlab/ex1/primesB.rb:9
         1  primesB_memprofile.rb:7

allocated objects by class
-----------------------------------
         2  Array
         2  String
         1  Hash
         1  PrimesB
         1  Range
         1  Thread::Mutex

retained memory by gem
-----------------------------------
     90112  other

retained memory by file
-----------------------------------
     89712  D:/git/algorithmlab/ex1/primesB.rb
       400  primesB_memprofile.rb

retained memory by location
-----------------------------------
     89712  D:/git/algorithmlab/ex1/primesB.rb:9
       360  primesB_memprofile.rb:8
        40  primesB_memprofile.rb:7

retained memory by class
-----------------------------------
     89712  Array
       240  Hash
       120  Thread::Mutex
        40  PrimesB

retained objects by gem
-----------------------------------
         4  other

retained objects by file
-----------------------------------
         3  primesB_memprofile.rb
         1  D:/git/algorithmlab/ex1/primesB.rb

retained objects by location
-----------------------------------
         2  primesB_memprofile.rb:8
         1  D:/git/algorithmlab/ex1/primesB.rb:9
         1  primesB_memprofile.rb:7

retained objects by class
-----------------------------------
         1  Array
         1  Hash
         1  PrimesB
         1  Thread::Mutex


Allocated String Report
-----------------------------------
         2  "9592"
         2  primesB_memprofile.rb:8


Retained String Report
-----------------------------------
9592
Total allocated: 1822152 bytes (8 objects)
Total retained:  800440 bytes (4 objects)

allocated memory by gem
-----------------------------------
   1822152  other

allocated memory by file
-----------------------------------
   1821672  D:/git/algorithmlab/ex1/primesC.rb
       480  primesC_memprofile.rb

allocated memory by location
-----------------------------------
   1021632  D:/git/algorithmlab/ex1/primesC.rb:12
    800040  D:/git/algorithmlab/ex1/primesC.rb:9
       440  primesC_memprofile.rb:8
        40  primesC_memprofile.rb:7

allocated memory by class
-----------------------------------
   1821632  Array
       240  Hash
       120  Thread::Mutex
        80  String
        40  PrimesC
        40  Range

allocated objects by gem
-----------------------------------
         8  other

allocated objects by file
-----------------------------------
         5  primesC_memprofile.rb
         3  D:/git/algorithmlab/ex1/primesC.rb

allocated objects by location
-----------------------------------
         4  primesC_memprofile.rb:8
         2  D:/git/algorithmlab/ex1/primesC.rb:12
         1  D:/git/algorithmlab/ex1/primesC.rb:9
         1  primesC_memprofile.rb:7

allocated objects by class
-----------------------------------
         2  Array
         2  String
         1  Hash
         1  PrimesC
         1  Range
         1  Thread::Mutex

retained memory by gem
-----------------------------------
    800440  other

retained memory by file
-----------------------------------
    800040  D:/git/algorithmlab/ex1/primesC.rb
       400  primesC_memprofile.rb

retained memory by location
-----------------------------------
    800040  D:/git/algorithmlab/ex1/primesC.rb:9
       360  primesC_memprofile.rb:8
        40  primesC_memprofile.rb:7

retained memory by class
-----------------------------------
    800040  Array
       240  Hash
       120  Thread::Mutex
        40  PrimesC

retained objects by gem
-----------------------------------
         4  other

retained objects by file
-----------------------------------
         3  primesC_memprofile.rb
         1  D:/git/algorithmlab/ex1/primesC.rb

retained objects by location
-----------------------------------
         2  primesC_memprofile.rb:8
         1  D:/git/algorithmlab/ex1/primesC.rb:9
         1  primesC_memprofile.rb:7

retained objects by class
-----------------------------------
         1  Array
         1  Hash
         1  PrimesC
         1  Thread::Mutex


Allocated String Report
-----------------------------------
         2  "9592"
         2  primesC_memprofile.rb:8


Retained String Report
-----------------------------------
