require_relative "primesB"
require 'profiler'

puts "\n==== PrimesB ==============\n"
Profiler__::start_profile

test = PrimesB.new(100000)
puts test.count_primes

Profiler__::stop_profile
Profiler__::print_profile(STDOUT)