require 'win32ole'

class Burndown
  attr_accessor :progresssummary, :milestones, :url, :tm
	
	def initialize(proj)
		@project = proj
		@milestones = Hash.new
		@url = "https://gitlab.com#{@project}\/milestones"
	end

	def readProject	
	# CONNECT TO THE MILESTONE LIST
		ie = WIN32OLE.new('InternetExplorer.Application')
		ie.visible = true
		ie.navigate(@url)
		sleep(3)
		ie.Document.All.each do |x|
			if x.innerHTML.gsub(/\n/," ") =~ /^\s*\<strong\>\<a\s+href\=\"#{@project}\/milestones\/(\d+)\"\>(.+?)\<\/a\>\<\/strong\>/
					@milestones[$1] = ["\"#{$2}\""]
		end
		end

	# GET PROGRESS STATS OF EACH MILESTONE
		@milestones.keys.each do |m|
			print "Processing Milestone [#{@milestones[m][0]}]"
			ie.navigate("#{@url}/#{m}")
			sleep(3)

			print " ... "
			openissues = 0
			closedissues = 0
			ie.Document.All.each do |x|
				if x.innerHTML =~ /\<div class="block issues"\>/
					if x.innerHTML =~ /Open:\s+(\d+)/
						openissues = $1.to_i
					end
					if x.innerHTML =~ /Closed:\s+(\d+)/
						closedissues = $1.to_i
					end
				end
			end
			@milestones[m] << openissues
			@milestones[m] << closedissues
			puts " Done"
		end
		puts "Processing completed\n\n"
		
	# Update the time mark to indicate completed processing
		@tm = Time.now.strftime("%Y/%m/%d")
		ie.quit
	end
	
	def progress(addlabel=0)
		if @tm.nil?
			readProject
		end
		if addlabel==1
		   outstring = '\"Date\"'
			 @milestones.length.times { |m|
			    outstring += ", \"M#{m+1}Open\",\"M#{m+1}Close\""
			 }
			 outstring += "\n#{@tm}"
		else
			outstring = @tm
		end
		
		@milestones.keys.each do |m|
			outstring += ", %3i,%3i" % [@milestones[m][1], @milestones[m][2]]
		end
		return outstring
	end
	
	def to_s
		if @tm.nil?
			readProject
		end
		openissues = 0
		closedissues = 0
		outstring = "Project Progress Report For [#{@project}] (Date: #{@tm})\n"

		outstring += "\n%-40s %5s %5s %6s\n" % 	["Milestone Description","Open","Clos", "% Closed"]	
		outstring += "=" * 60 + "\n"
		closedmilestones = 0
		@milestones.keys.each do |m|
		  completion = (@milestones[m][2] *100.0) / (@milestones[m][1] +@milestones[m][2])
			if (@milestones[m][2] > 0) && (@milestones[m][1] == @milestones[m][2])
			   closedmilestones += 1
			end
			
			outstring += "%-40s %5i %5i %6.2f\n" % 
				[@milestones[m][0].gsub(/\"/,""), @milestones[m][1], @milestones[m][2], completion ]
			openissues += @milestones[m][1]
			closedissues += @milestones[m][2]
		end
		outstring += "=" * 60
		outstring += "\n%-40s %5i %5i %6.2f%% (Milestones: %.2f%%)\n" % 
				["All Milestones",openissues,closedissues,
				closedissues * 100.0 / (openissues + closedissues), 
				(closedmilestones * 100.0)/(@milestones.length)]
		return outstring
	end
end


# This assumes that you have already logged into GitLab
# Using IE

proj = Burndown.new("/rbatzing/algorithmlab")
puts proj
open("progressreport.txt","w") {|f|  f.puts proj }
puts "\nLog Entry:"

outstring = "Date        "
proj.milestones.length.times {|m|
  outstring += " M%02i O/C " % [m+1]
}
puts outstring
puts "-" * 11 + "  -------" * (proj.milestones.length)
puts proj.progress
# First time run as: open("progress.log","a") {|f|  f.puts proj.progress(1) }
open("progresslog.csv","a") {|f|  f.puts proj.progress }

__END__

