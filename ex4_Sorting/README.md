# Experiment 3: Sorting

## Research Question:

How different is the performance of different sort algorithms

## Classes:

* **Sorting** contains all the methods need to create different kinds of arrays and sort them

## Methods:

### Array utilities

* changePivot(lo,hi) determine whether the first, last or middle value would make the best pivot for quicksort
* initialize(n,type=0) creates an array of n elements. Ordering type:  -1:descending; 0:random; 1:ascending 
* shuffle: randomize the order of elements in an array
* swap(p1,p2): swap the data between 2 positions
* to_s: print out the sorted array

### Sorting methods

* bubbleSort
* insertionSort
* quickSort(from=0, to=nil)  Self priming recursive version of quicksort


## Attributes:

* @array: test array
* @num size of the test array

## Experiment Guidelines

1. Create a benchmark for comparing bubbleSort, insertSort and quickSort

2. Determine the Big O characteristics of the 3 sorting methods in respect to the following parameters:

   * Size of Array:  100, 1000,  10000, 100000
   * Type of Array: Ascending, Descending and Random

3. Remove the comment characters lines 36 and 120 and determine the effect on performance

4. Create the graphs and submit your findings using LaTeX
