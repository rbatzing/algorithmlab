class Sorting
	attr_accessor :array, :num
	
	def initialize(n,type=0)
		@num = n
	  @array = Array.new
	  n.times do |i|
			case type
			when type < 0
				@array << n - i
			when type > 0
				@array << 1 + i
			else
				@array << rand(n*100)
			end
		end
	end
	
	def to_s
		out = "Array: "
		@array.each do |i|
			out += ("%4i " % [i])
		end
		return out
	end
	
	def quickSort(from=0, to=nil)
    if to == nil
      to = @array.count - 1
    end

    if from >= to
      return
    end
		
#		changePivot(to,from)
    pivot = @array[from]
    min = from
    max = to
    free = min

    while min < max
      if free == min
        if @array[max] <= pivot
					@array[free] = @array[max]
          min += 1
          free = max
        else
          max -= 1
        end
      elsif free == max 
        if @array[min] >= pivot
          @array[free] = @array[min]
          max -= 1
          free = min
        else
          min += 1
        end
      else
        raise "Inconsistent state"
      end
    end

    @array[free] = pivot
    quickSort from, free - 1
    quickSort free + 1, to
	end

	def swap(p1,p2)
		@array[p1], @array[p2] = @array[p2], @array[p1]
	end
	
	def shuffle
		@array.shuffle!
	end
	
	def changePivot(lo,hi)
		mid = (lo + hi) / 2
		if (@array[lo] < @array[mid]) && (@array[mid] < @array[hi])
			swap lo, mid
		elsif (@array[hi] < @array[mid]) && (@array[mid] < @array[lo])
		  swap lo, mid
    elsif (@array[lo] < @array[hi]) && (@array[hi] < @array[mid])
			swap lo, hi
		elsif (@array[mid] < @array[hi]) && (@array[hi] < @array[lo])
			swap lo, hi
#		elsif (@array[mid] < @array[lo]) && (@array[lo] <= @array[hi])
#		  swap lo,lo
#		elsif (@array[hi] < @array[lo]) && (@array[lo] <= @array[mid])
#			swap lo,lo
		end
	end

	def insertionSort
		i = 0
		while i < @array.size
			x = @array[i]
			j = i - 1
			while (j >= 0) && (@array[j] > x)
        @array[j+1] = @array[j]
        j -= 1
			end
			@array[j+1] = x
			i += 1
		end
	end
	
	def bubbleSort
		i = @array.size - 1
		while i > 0
			j = 0
			changed = false
			while j < i
				if @array[j] > @array[j+1]
					swap j, j + 1
					changed = true
				end
				j += 1
			end
#			return if !changed
			i -= 1
		end
	end
end

if __FILE__ == $0
	a = Sorting.new(12)
	puts a
	a.quickSort
	puts a
	a.shuffle
	a.insertionSort
	puts a
	a.shuffle
	a.bubbleSort
	puts a
end