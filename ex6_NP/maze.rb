class State
# Maze nodes
#
#  n0, n1, n2,
#  n3, n4, n5,
#  n6, n7, n8
#
	attr_accessor :node

	def initialize(nlist)
		@node = Array.new
		nlist.each do |n|
			@node << n
		end
	end
	
	def distr2(num,a,b)
		num.times do
			if rand() < 0.5
				node[a] += 1
			else
				node[b] += 1
			end
		end
	end

	def distr2hard(num,a,b)
		num.times do
			if rand() < 0.9
				node[a] += 1
			else
				node[b] += 1
			end
		end
	end

	
	def distr5(num)
		num.times do
			case rand(5)
			when 0 then @node[1] += 1
			when 1 then @node[3] += 1
			when 2 then @node[5] += 1
			when 3 then @node[7] += 1
			when 4 then @node[8] += 1
			end
		end
	end
	
	def distr5hard(num)
		num.times do
			case rand(9)
			when 0 then @node[7] += 1
			when 1..2 then @node[0] += 1
			when 3..4 then @node[2] += 1
			when 5..6 then @node[3] += 1
			when 7..8 then @node[5] += 1
			end
		end
	end
	
	def update
		newgen = State.new([0,0,0,0,0,0,0,0,0])
		newgen.node[4] = @node[0] + @node[2] + @node[6]
		newgen.node[8] = @node[8]
		newgen.distr2(@node[1],0,2)
		newgen.distr2(@node[3],0,6)
		newgen.distr2(@node[5],2,8)
		newgen.distr2(@node[7],6,8)
		newgen.distr5(@node[4])
		@node = newgen.node 
	end
	
	def updatehard
		newgen = State.new([0,0,0,0,0,0,0,0,0])
		newgen.node[4] = @node[0]
    newgen.node[0] = @node[2] + @node[6]
		newgen.node[8] = @node[8]
		newgen.distr2hard(@node[1],0,2)
		newgen.distr2hard(@node[3],0,6)
		newgen.distr2hard(@node[5],0,2)
		newgen.distr2hard(@node[7],6,8)
		newgen.distr5hard(@node[4])
		@node = newgen.node 
	end
	
	def completed
		return @node[8]
	end
	
	def to_s
		output = "Values: ["
		@node.each do |n|
		 output += "#{n}, "
		end
		output += "]"
		return output
	end
end

class Maze
	attr_accessor :number, :state, :update

	def initialize(num)
		@state = State.new([num,0,0,0,0,0,0,0,0])
		@number = num
		@update = 0
#		puts "#{@number} - #{@state}"
	end
	
	def solve
		while @state.completed < @number
			@state.update
			@update = @update + 1
#			puts "#{@update} #{@state}"
		end
		return @update
	end

	def solvehard
		while @state.completed < @number
			@state.updatehard
			@update = @update + 1
#			puts "#{@update} #{@state}"
		end
		return @update
	end
end

runs = Array.new
1000.times do
	m = Maze.new(1)
  runs << m.solve
end
puts "#{runs}"

__END__

