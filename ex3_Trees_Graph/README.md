# Experiment 3: Trees an Graphs

This experiment is inspired by the 13 boys of the Wild Boars Soccer Team trapped in the Tham Luang Cave in Chiang Rai by rising water from torrential rain. The flooded cave has many junctions and deadends which had been extensively mapped and published by a spelunker. It took rescuers working around the clock 10 days to find them within the narrow, dark, flooded passage and another 9 days to extract them to safety in what proved to be a heroic around the clock race against time and flood. The task force led by the governor of Chiang Rai had only a small window of opportunity to consider options and marshalled the resources needed for an extremely complex operation involving police, military, government ministries, volunteer organizations, international experts and a hundreds of volunteers providing support to the rescue effort. The successful location and rescue of the boys was nothing short of a miracle.

The rescue effort would have been much harder if the cave had been uncharted. This experiment attempts to use 3 algorithms for tree and graph transversals to exploring all the rooms of uncharted, complex cave networks of 2 types: one represented by a tree with many deadends and other a graph with multiple chains of loops. Each cave has only one entrance. This study will attempt to measure the performance of each algorithm in the finding and mapping all the chambers of these  randomly-generated caves.

* A breadth-wise search
* A adepth-wise search
* Dijisktra's algorithm

## Research Question:

* What is the differences in the performance of these algorithms in mapping arbitrary networks of caves?
* Which cave structure is easier to map?

## Classes:
* Nodes: representing the rooms of the cave
* Network: representing the cave system navigate itand means to map and 

## Methods:

## Attributes:

## Experiment Guidelines:

1) Run the cave search program using caves of different sizes: i.e., 10, 30, 100,300 chambers

2) Plot a graph to show what happens to efficiency as the number of chambers increases

3) Plot a graph to show the number of chambers found over time for each of the cave types and for each search algorithm.

4) Describe and explain the differences found.

5) Suggest which search algorithm would be best for each type of cave.
