class Link
  attr_accessor :nodeA, :nodeB
   
  def initialize(node1,node2)
    if node1.id < node2.id
      @nodeA = node1
      @nodeB = node2
    else
      @nodeA = node2
      @nodeB = node1
    end
  end
  
  def eql?(link)
    return (@nodeA == link.nodeA &&
        @nodeB == link.nodeB)
  end
  
  def to_s
    return "#{@nodeA} -> #{@nodeB}"
  end
  
  def destination(startNode)
    if startNode == @nodeA
        return @nodeB
    else
        return @nodeA
    end
  end
end

class Node
  attr_accessor :id, :links 
	
  def initialize(id)
	  @id = id
		@links = []
	end
	
	def addlink(node)
    @links << Link.new(self,node)
		node.links << @links.last		
	end
	
	def to_s
	  return "#{@id}"
	end
	
	def neighborhood
		neighbors = []
		@links.each do |link|
		  neighbors << link.destination(self)
		end
		return neighbors
	end
end

class CaveSystem
  attr_accessor :root, :visited, :explored, :discoverypath, :searchtype
	
	def initialize
		
		@@nodes = []
		@@nodes << Node.new(0)
		@searchtype = "??"
		@nodesFound = []
		@visitedLinks
		@root = @@nodes.last
	end
	
	def add2cave
		@@nodes << Node.new(@@nodes.length)
		visited = []
		node = @root
		while node.links.length > 0
			if (node.links.length < 4) && (rand() < 0.25)
				node.addlink(@@nodes.last)
				return
			else
				node = node.links.sample.destination(node)
			end
		end
		node.addlink(@@nodes.last)
	end
	
	def to_s 
		graph = CavePlot.new
		explored = []
		visited = []
		level = 0
		nextnode = [[@root,level]]
		while nextnode.length > 0 do
		  nodegroup = nextnode.shift
		  node = nodegroup[0]
		  lvl = nodegroup[1]
		  
		  if !node.nil? 
			# && visited.find_index(node).nil?
				graph.add(node,lvl)
				
				if visited.find_index(node).nil?
				  visited << node				
				  node.links.each do |lnk|
					  if explored.find_index(lnk).nil?
							explored << lnk
							nextnode.unshift([lnk.destination(node),lvl+1])
						end
					end
				end
		  end
		end
		
		graph.update
		return graph.to_s
	end

	def depthwise
	  @searchtype = "Depth-wise"
		@visited = []
		@explored = []
		@discoverypath = dodepthwise(@root)
	end
	
	def dodepthwise(node)
		out = "#{node.id}"
		if @visited.find_index(node).nil?
			@visited << node
			node.links.each  do |lnk|
				if @explored.find_index(lnk).nil?
					@explored << lnk
					if @visited.find_index(lnk.destination(node)).nil?
						out += "->"
					else
						out += "=>"
					end
					out += dodepthwise(lnk.destination(node))
					out += "=>#{node.id}"
				end
			end
		end
		return out
	end
	
	def tripAnalysis
	  tripReduce
		puts "Search type: #{@searchtype}"  + ("-" * 30)
		text = discoverypath.dup
		puts "Path: #{text}"
	  discover = []
		stepcount = 0
		while text =~ /([\+\=\-])\>/
			mark = $1
			stepcount += 1
			if mark.eql?("-")
			  discover << stepcount
			end
		  text.sub!(/[\+\=\-]\>/,"")
		end

		puts "Efficiency: #{discover.length}/#{stepcount} = #{1.0*discover.length/stepcount}"
		puts "#{discover.inspect}"
	end
	
	def breadthwise
		@searchtype = "Breadthwise Search "
		@visited = []
		@explored = []
		workqueue = [[@root,"",""]]
		@discoverypath = ""
		while workqueue.length > 0
		  n = workqueue.shift
			node = n[0]
			pre = n[1]
			post = n[2]
			if !pre.eql?("")
			  if @visited.find_index(node).nil?
					pre += "->"
				else
					pre += "+>"
				end
				@discoverypath += "#{pre}#{node.id}#{post}"
			end
			if @visited.find_index(node).nil?
				@visited << node
				pre.gsub!(/\-/,"+")
				node.links.each  do |lnk|
					if @explored.find_index(lnk).nil?
						@explored << lnk
						workqueue << [lnk.destination(node),"#{pre}#{node}","=>#{node}#{post}|"]
					end
				end
			end
		end
		@discoverypath
	end
	
	def tripReduce 
	t = @discoverypath.gsub(/\|+/,"|")
  while t =~ /((.+)\=\>(\d+)\=\>(\d+)\|\4\+>\3(\D)(.+))/
	 t = "#{$2}=>#{$3}\|#{$3}#{$5}#{$6}"
	end
  puts "----"
	t.gsub!(/\|\d*/,"")
	@discoverypath = t
	end
	
end

class CavePlot
attr_accessor :map

	def initialize
		@map = []
	end
	
	def add(node,level)
		@map << "#{"|  " * level}+--o Node #{node}"
	end
	
	def update
		@map.last.gsub!(/\|/," ")
		row = @map.length - 2
		while row > 0
			pos = 0
			until (@map[row][pos]).eql?("+")
			  if (@map[row][pos]).eql?("|")
				  if !((@map[row+1][pos]).eql?("|") || (@map[row+1][pos]).eql?("+"))
						@map[row][pos] = " "
					end
				end
				pos += 1
			end
			row -= 1
		end
	end
	
	def to_s
		return @map.join("\n")
	end
end

class TreeCave < CaveSystem
  def initialize(num)
	  super()
	  num.times do
		  add2cave
		end
  end
	
	def to_s
	  out = "Tree Cave Print " + ("=" * 20) + "\n"
		out += super()
	end
end

class GraphCave < CaveSystem
  def initialize(num)
	  super()
	  num.times do
		  add2cave
		end
		createloops
  end
	
	def	createloops
	  leafs = []
		@@nodes.each do |n|
		  if n.links.length < 2
			   leafs << n
			end
		end
		(leafs.length/2).to_i.times do
			n1 = leafs.sample
			leafs.delete(n1)
			n2 = leafs.sample
			leafs.delete(n2)
			puts "Combining #{n1} with #{n2}"
			n1.addlink(n2)
		end
	end
	
	def to_s
	  out = "Graph Cave Print " + ("=" * 20) + "\n"
		explored = []
		@@nodes.each do |n|
		  rowtxt = ""
			n.links.each do |l|
				if explored.find_index(l).nil?
					explored << l 
					rowtxt += "N#{l.destination(n).id},"
				end
			end
			if rowtxt.length > 0
				out += "N#{n.id}--[" + rowtxt.chomp(",") + "]\n"
			end
		end
		return out
	end
end

class GridCave < CaveSystem
	attr_accessor :width, :depth

  def initialize(width,depth)
		super()
		@width = width
		@depth = depth
		(@depth * @width).times do 
				@@nodes << Node.new(@@nodes.length)
		end
		@depth.times do |d|
		  @width.times do |w|
				
				if d == 0
					@root.addlink(@@nodes[d* @width + w + 1])
				else
				  @@nodes[(d-1)* @width + w + 1].addlink(@@nodes[d* @width + w + 1])
				end
				
				if w > 0
					@@nodes[d * @width + w].addlink(@@nodes[d * @width + w + 1])
				end
			end
		end
	end
	
	def to_s
	  out = "Grid Cave Print (#{@width}x#{@depth}) " + ("=" * 10) + "\n\n"
		@depth.times do |d|
			if d == 0
				out += ("[%03i]\n" % [@root.id])
				out += "  |\n"
				out += "  +" + ("-----+" * (@width -1)) +"\n"
			end
			out += ("  |   " * @width) + "\n"
			@width.times do |w|
				if w < 1
					out += ("[%03i]" % [@@nodes[d* @width + w + 1].id])
				else
					out += ("-[%03i]" % [@@nodes[d* @width + w + 1].id])
				end
			end
			out += "\n"
		end
		return out 
	end
end

5.times do
[10,30,100,300].each do |number|
treecave = TreeCave.new(number)
puts "Tree Cave (Number: #{number}) " + "#" * 30
treecave.depthwise
treecave.tripAnalysis
treecave.breadthwise
treecave.tripAnalysis

puts "\n" + ("=" * 40 ) + "\n"

graphcave = GraphCave.new(number)
puts "Graph Cave (Number: #{number}) "  + "#" * 30
graphcave.depthwise
graphcave.tripAnalysis
graphcave.breadthwise
graphcave.tripAnalysis

puts "\n" + ("=" * 40 ) + "\n"

gridcave = GridCave.new(5,number / 5)
puts "Grid Cave (5 x #{number / 5}, Number: #{number}) " + "#" * 20
gridcave.depthwise
gridcave.tripAnalysis
gridcave.breadthwise
gridcave.tripAnalysis

puts "\n" + ("=" * 40 ) + "\n"

end
end

__END__


