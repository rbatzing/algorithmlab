class Node
  attr_accessor :neighbors, :backlink, :id
	
  def initialize(id)
	  @id = id
		@neighbors = []
	end
	
	def addlink(node)
	  @neighbors << node
		node.backlink = self
	end
	
	def to_s
	  return "#{@id}"
	end
	
	def neighborhood
		out = "{"
		if @neighbors.length < 1
		  out += "*"
		else
		  first = true
			@neighbors.each do |n|
			  if first
				  out = out + "#{n.to_s}"
					first = false
				else
				  out = out + ",#{n.to_s}"
				end
			end
		end
		out += "}"
	  return out
	end
	
	def backlink_as_neighbor
	  if @neighbors.find_index(@backlink).nil? && !@backlink.nil?
		  @neighbors << @backlink
			@neighbors.sort_by!{|e| e.id}
		end
	end
	
end

class CaveSystem
  attr_accessor :root, :visited, :nextnode, :nodeorder, :nodes 
	
	def initialize
		@nodes = []
		createcavern
		@root = @nodes.last
	end
	
	def createcavern
		@nodes << Node.new(@nodes.length)
	end
	
	def add2cave
		createcavern
		node = @root
		while node.neighbors.length > 0
			if (node.neighbors.length < 4) && (rand() < 0.25)
				node.addlink(@nodes.last)
				return
			else
				node = node.neighbors.sample
			end
		end
		node.addlink(@nodes.last)
	end
	
	
	def addbacklinks
		@nodes.each do |n|
		  n.backlink_as_neighbor
		end
	end
	
	def to_s
		out = ""
		visited = []
		nextnode = [@root]
		i = 0
		while nextnode.length > 0 do
		  node = nextnode.shift
			if !node.nil? && visited.find_index(node).nil?
				out += "\tn#{node} --> #{node.neighborhood};\n"
				i += 1
				if (i % 4) == 0
				  out += "\n"
				end
				visited << node
				node.neighbors.each do |n|
					nextnode << n if visited.find_index(n).nil?
				end
			end
		end
		return out
	end
	
	def bredthwise
	  out = "Bredthwise Path:\n  ->"
		@nodeorder = Array.new(@nodes.length,0)
		@nodecount = 0
		@visited = []
		@nextnode = [[@root,"",""]]

		while @nextnode.length > 0 do
		  node = @nextnode.shift
		  if !node.nil? &&
					@visited.find_index(node[0]).nil?
				out += "#{node[1]} "
				@nodeorder[node[0].id] = @nodecount
				@nodecount += 1
				
				pre = "#{node[1]} #{node[0]} |>"
				if node[0].id != 0
					post = "=> #{node[0]} #{node[2]}"
				else
				  post = "=>"
				end
				@visited << node[0]
				node[0].neighbors.each do |n|
				  if @visited.find_index(n).nil?
					  if n.neighbors.length > 1  
							@nextnode << [n,pre,post]
						else
						  @visited << n
							@nodeorder[n.id] = @nodecount
							@nodecount += 1
						end
						out += "#{node[0]} -> #{n} => "
					end
				end
				if node[0].id != 0
			     out += "#{node[0]}  #{node[2]}"
				end
			end
    end
		out += " 0"
		return out
  end

	def showorder
		out = "  Order of Discovery:"
		@nodeorder.each_with_index do |n,i|
			out += "\tn#{i}[\"" + ("%0.3f" % [0.8 * n / @nodeorder.length]) + "\"];"
			if (i % 4) == 0
				out += "\n"
			end
		end
		out += "\n"
		return out
	end
	
	def showvisited
	  out ="["
		@visited.each do |v|
		  out += ",#{v.id}"
		end
		out += "]"
		return out
	end
	
  def depthwise
		@visited = []
		@nodeorder = Array.new(@nodes.length,0)
		@nodecount = 0
	  return "Depthwise Path: #{do_depthwise(@root)}"
 end

  def do_depthwise(node)
	  out = ""
	  if @visited.find_index(node).nil?
      out = " -> #{node}"
			@nodeorder[node.id] = @nodecount
			@nodecount += 1

		  @visited << node
		  node.neighbors.each do |nextnode|
        if (@visited.find_index(nextnode).nil?)
					out += do_depthwise(nextnode) 
					out += " => #{node}"
				end
		  end
		end
		return out
	end
end

class TreeCave < CaveSystem
  def initialize(num)
	  super()
	  num.times do
		  add2cave
		end
		addbacklinks
  end
	
	def to_s
	  out = "Tree Cave Size: #{@nodes.length} " + ("=" * 20) + "\n"
		out += super()
	end
end

class GraphCave < CaveSystem
  def initialize(num)
	  super()
	  num.times do
		  add2cave
		end
		createloops
		addbacklinks
  end
	
	def	createloops
	  leafs = []
		@nodes.each do |n|
		  if n.neighbors.length < 1
			   leafs << n
			end
		end
		while (leafs.length) > 1 do
			n1 = leafs.sample
			leafs.delete(n1)
			n2 = leafs.sample
			leafs.delete(n2)
			puts "Combining #{n1} with #{n2}"
			n1.neighbors << n2
			n2.neighbors << n1
		end
	end
	
	def to_s
	  out = "Graph Cave Size: #{@nodes.length} " + ("=" * 20) + "\n"
		out += super()
	end
end

class GridCave < CaveSystem
  attr_accessor :cave_width, :cave_depth
	
  def initialize(cave_width,cave_depth)
	  super()
		@cave_width = cave_width
		@cave_depth = cave_depth
    @cave_depth.times do |depth|
		  offset = 1 + ((depth-1) * @cave_width)
		  @cave_width.times do |width|
				@nodes << Node.new(@nodes.length)
				if depth == 0
		      node = @root
				else
				  node = @nodes[offset + width]
				end
				puts "#{offset} #{node.id} #{@nodes.last.id}"
				
				node.addlink(@nodes.last)
				if width > 0
				  n=@nodes.last(2)
					n[0].neighbors << n[1]
					n[1].neighbors << n[0]
				end
			end
		end
		addbacklinks
		rev_neighbors
  end
	
	def rev_neighbors
	  @nodes.each do |n|
		  n.neighbors.reverse!
		end
	end

	def to_s
	  out = "Grid Cave Size: #{@nodes.length} (#{@cave_width} x #{@cave_depth})" + ("=" * 10) + "\n"
		out += super()
	end
end

treecave = TreeCave.new(30)
puts treecave
puts treecave.bredthwise
puts treecave.showorder
puts treecave.depthwise
puts treecave.showorder
puts

graphcave = GraphCave.new(30)
puts graphcave
puts graphcave.bredthwise
puts graphcave.showorder
puts graphcave.depthwise
puts graphcave.showorder
puts

gridcave = GridCave.new(5,6)
puts gridcave
puts gridcave.bredthwise
puts gridcave.showorder
puts gridcave.depthwise
puts gridcave.showorder
puts

__END__

Need to add the  @followedlink to check all passageways in the cave 