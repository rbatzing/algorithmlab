#LOAD RELEVANT CLASS LIBRARIES
require_relative "looptest"
require 'benchmark'
include Benchmark

5.times do |runnum|
# LIST OF FUNCTIONS TO BE TESTED
functlist = [ "A2","A3","A4","A5",
	"B1",	"B2",	"B3",	"B4",	"B5",
	"C", "D",	"E" ]
	
# OPEN EXPERIMENTAL RESULTS FILE
fout = open("results#{runnum}.csv","w")
	
# SETUP THE BENCHMARK TEST
puts "LoopTest Benchmark " + "=" * 36
fout.puts "loopname,num,count,runtime"
bm(12) do |x|

# TEST RELEVANT RANGE OF num (3,10,30,...)
n = 1
until functlist.length < 1
	num = (10.0**(0.5*n)).to_i
	unit = LoopTest.new(num)


	# TEST ALL ACTIVE LOOP FUNCTIONS
	activeFunct = functlist.dup
	activeFunct.each do |name|
		count = 0
		y = x.report("%3s %4d " % [name,num]){
			count = unit.do(name)
		}
		
		# EXTRACT THE RUNTIME FROM THE BENCHMARK
		runtime = y.to_s.
			sub(/(\S+\s+){3}/,"").
				gsub(/[\( \)]/,"").
					to_f

		# SAVE EXPERIMENTAL RESULTS
		fout.puts("%s,%i,%i, %.6f" % 
			[name,num,count,runtime])

		# REMOVE FUNCTIONS W RUNTIMES > 1.0 SEC							
		if runtime > 1.0
			functlist.delete(name)
		end
	end
	
	# INCREMENT VALUE OF num 
	n += 1
	end		
end
fout.close
end