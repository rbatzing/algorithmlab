class LoopTest
  attr_accessor :max, :count
  
  def initialize(max)
		@max = max
	end
	
	def test
	   @count += 1
	end
	
	def loopA5
	  @count = 0
		max.times do test end
		max.times do test end
		max.times do test end
		max.times do test end
		max.times do test end
		return @count
	end
	
	def loopA4
	  @count = 0
		max.times do test end
		max.times do test end
		max.times do test end
		max.times do test end
		return @count
	end
	
	def loopA3
	  @count = 0
		max.times do test end
		max.times do test end
		max.times do test end
		return @count
	end
	
	def loopA2
	  @count = 0
		max.times do test end
		max.times do test end
		return @count
	end

	def loopB5
	  @count = 0
		max.times do
			max.times do
				max.times do 
					max.times do 
						max.times do test end
					end
				end
			end
		end
		return @count
	end
	
	def loopB4
	  @count = 0
		max.times do
			max.times do
				max.times do 
					max.times do test end
				end
			end
		end
		return @count
	end

	def loopB3
	  @count = 0
		max.times do
			max.times do 
				max.times do test end
			end
		end
		return @count
	end

	def loopB2
	  @count = 0
		max.times do
				max.times do test end
		end
		return @count
	end
	
	def loopB1
	  @count = 0
		max.times do test end
		return @count
	end
	
	def loopC
		@count = 0
		max.times do |w|
			(w+1).times do |x|
				(x+1).times do |y|
					(y+1).times do |z|
						(z+1).times do test end
					end
				end
			end
		end
		return @count
	end

	def loopD
		@count = 0
		max.times do
			max.times do 
				max.times do test end
			end
			max.times do 
				max.times do test end
			end
		end	 
		return @count
	end
	
	def loopE
		@count = 0
		max.times do |x|
			(x+1).times do |y|
				(y+1).times do test end
			end
			(x+1).times do |y|
				(y+1).times do test end
			end
		end
		return @count
	end
	
	def do(unit)
		case unit
		when "A2" then return loopA2
		when "A3" then return loopA3
		when "A4" then return loopA4
		when "A5" then return loopA5
		when "B1" then return loopB1
		when "B2" then return loopB2
		when "B3" then return loopB3
		when "B4" then return loopB4
		when "B5" then return loopB5
		when "C" then return loopC
		when "D" then return loopD
		when "E" then return loopE
		end
	end
end

if __FILE__ == $0
	5.times do |n|
		num = (10.0**(0.5*(n+1))).to_i
		STDERR.puts num
	  unit = LoopTest.new(num)
		puts "A2 #{num} #{unit.loopA2}"
		puts "A3 #{num} #{unit.loopA3}"
		puts "A4 #{num} #{unit.loopA4}"
		puts "A5 #{num} #{unit.loopA5}"

		puts "B1 #{num} #{unit.loopB1}"		
		puts "B2 #{num} #{unit.loopB2}"
		puts "B3 #{num} #{unit.loopB3}"
		puts "B4 #{num} #{unit.loopB4}"
#		puts "B5 #{num} #{unit.loopB5}"
		puts "C #{num} #{unit.loopC}"
		puts "D #{num} #{unit.loopD}"
		puts "E #{num} #{unit.loopE}"
		STDOUT.flush

	end
end

__END__


