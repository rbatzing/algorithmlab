require 'benchmark'
include Benchmark

puts "\nRuby Math op comparison " + "=" * 36
a = 2
10.times do
	bm(3) do |x|
		x.report("="){100_000.times{  a;a;a;a;a }}	
		x.report("+"){100_000.times{  a + a + a + a + a + a}}
		x.report("-"){100_000.times{  a - a - a - a - a - a}}
		x.report("x"){100_000.times{  a * a * a * a * a * a}}
		x.report("/"){100_000.times{  a / a / a / a / a / a}}
		x.report("%"){100_000.times{  a % a % a % a % a % a}}
		x.report("^"){100_000.times{  a ^ a ^ a ^ a ^ a ^ a}}
	end
end
