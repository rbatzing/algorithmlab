# Experiment 2: Loops and Big O

Compare the Big O characteristics of loops with different structures 

## Research Question:

     How much impact does the structure of loops have on the Big O characteristics?
	 
	 Specifically, how do the structure of the main loop in key functions of an application effect scalability?

## Classes:

This research focuses on 1 Ruby class with a series of 5 test methods.

* **LoopTest:** a class with methods that implement 5 different types of loops.
* **Op_Test:** a benchmark for basic math operators

## Methods:

* **loopA:** Sequential series of loops
* **loopB:** Nested loops
* **loopC:** Nested limited loops
* **loopD:** Nested sequential series of loops
* **loopE:** Nested limited loops

## Attributes:

* **@max:** The max value the iterations of loop structure.

## Experiment Guidelines

1. Open a new project in Overleaf. Describe the research question and why you think this is important for a software developer. (Introduction)

2. Write a pseudocode description of each loop method. (Introduction)

3. Examine the 5 loops and determine the mathematical formula to determine the counts produced for each size of max.(Introduction)

4. Add a series of test functions to determine the number of times the innermost loops are repeated for max = (3,10,32, 100,316,1000) . (Methodology)

5. Create and run a benchmark test to test runtimes for each size of max for each loop structure. (Methodology) 

6. Compare the experimental data in Step 3 to the calculated data from Step 4. (Results)
7. Compare the runtime of math operators (Results)

8. Plot the runtimes against the size of max for each loop structure. (Use a log-log plot)  and determine the Big O characteristics. (Results)
9. Summarize the effect of loop structure on Big O behavior of functions. (Discussion)
10. Discuss what the relative effect the different math operators would have on runtime of a loop within an aplication. (Discussion)

11. Add Title, Author list, abstract, keywords and bibliography to the paper and summit as a url to the share view.
