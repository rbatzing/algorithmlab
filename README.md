# Algorithm Lab

------

<img src="https://gitlab.com/rbatzing/algorithmlab/raw/master/projectlogo.png" width="20%"  align="right">

A series of experiments to demonstrate some key aspects of algorithms. This is part of the instruction provided in CS202 ALGORITHMS DESIGN AND ANALYSIS, a course taught in in Semester 1/2018 at Payap University, Faculty of Science Department of Computer Science.

This material is the product of collaborative effort between Mr Nutawud Boonroadwongs and Dr. Robert Batzinger

All inquires should be addressed to robert_b@payap.ac.th or mailed to 

> Dr Robert Batzinger    
> Payap University    
> Department of Computer Science    
> Faculty of Science    
> Amphur Muang, Chiang Mai 50000    
> Thailand


## Report Format

All research reports should be done in the Latex style given at https://www.overleaf.com/read/fwwkdpzjfhzw

<img src=progress.png align=right width=40%>

## Experiments

### [Experiment 1. Time studies in Ruby](ex1/README.md) 

This is a comparison of 3 algorithms designed to find prime numbers and adjacent prime neighbors which are prime number separated by 2. While the output is identical for all algorithms, the performance and memory requires are not.

### [Experiment 2. Exploring the Big O in Loops](ex2/README.md)

### [Experiment 3. Trees and Graphs](ex3/README.md)

### [Experiment 4. Sorting](ex4/README.md)

### [Experiment 5. 5. Dynamic vs Functional Programming](ex5/README.md)

### [Experiment 6. NP Hard: Rats in a maze](ex6/README.md)
<img src="https://gitlab.com/rbatzing/algorithmlab/raw/master/projectlogo.png" width="50%"  align="right">
This NP-hard problem attempts to predict the transit time for rats in this maze.
Assuming all switching is done randomly, the goal is to 
determine the number of time units required for 1000 rats
to move through the maze.

* All mice enter in the upper left green entry node.
* All mice exit via the red node and do not return.
* The yellow nodes randomly switch the rats between 2 paths (lines in gold).
* The purple node randomly switch the rats between 5 outgoing paths (lines in purple).
* All green nodes lead back to the purple node (lines in green).

While this problem is indeterminate, statistics can be used to give a probabilistic range of values. 


# FAQ

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

   1. Setup an account and a project at Gitlab
   
   2. Download and establish git on your computer gitforwindows.com
   
      * Use notepad++ as your editor
	  * Use bash as your terminal
	  * Line endings should be submit as ASIS but upload as LF only
	  
   3. Configure your version of git 
	
	  git config --global user.name "John Doe"
      git config --global user.email johndoe@example.com
      git config --global core.editor notepad++

   4. If you are using computer that someone has used, you make need to delete the .ssh files and the security certificates of the Windows system before you can login remotely.

   5. Clone a project created in GitLab

     git clone https://gitlab.com/rbatzing/cs202.git
	 git add -A
	 git push origin master

   6. Establish an alternative remote to allow updating from class

     git remote add class https://gitlab.com/rbatzing/algorithmlab.git
	 git pull --allow-unrelated-histories class master

   7. Establish an alternative remote to allow updating from OverLeaf
   
     git remote add overleaf https://git.overleaf.com/12737267hcfctztwprky
     git pull --allow-unrelated-histories overleaf master
	 git status
	 git reset HEAD

* Dependencies

   * Ruby
   * Git
   * LaTeX
   * R
 
* How to run tests
* Deployment instructions
-
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* @rbatzing
* @donut
